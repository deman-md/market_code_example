package com.xlvsan.mihonya.model

class ProductsInitResponse() {
    var products: List<Product>? = null

    constructor(products: List<Product>?) : this() {
        this.products = products
    }
}