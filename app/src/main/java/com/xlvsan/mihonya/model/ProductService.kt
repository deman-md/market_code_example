package com.xlvsan.mihonya.model

import android.annotation.SuppressLint
import android.content.Context
import com.google.gson.Gson
import com.xlvsan.mihonya.model.ProductsServiceInterface.*
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import kotlinx.coroutines.runBlocking
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Handler
import java.util.concurrent.locks.ReentrantLock


class ProductService(context: Context) : ProductsServiceInterface {


    private val gson = Gson()

    private val initUrl =
        "https://gist.githubusercontent.com/alegch/c1241c81e042d83c78caf38fc525ca04/raw/493b1b3dee02f4b896bd81096bdecaf5931c0332/products.json"

    override val onProductsChangedListeners: MutableList<OnProductsChangedListener> = ArrayList()
    override val onProductChangedListeners: MutableList<OnProductChangedListener> = ArrayList()
    override val onAvailableProductsChangedListeners: MutableList<OnProductsChangedListener> = ArrayList()


    private var products: MutableSet<Product> = mutableSetOf(Product("adf", "234", 123))
    private val preferences: SharedPreferences = context.getSharedPreferences("PRODUCTS_SERVICE", MODE_PRIVATE)
    private val PRODUCTS_PREF = "PRODUCTS"

    private val editLock: ReentrantLock = ReentrantLock()


    init {
        if (preferences.getString(PRODUCTS_PREF, null) == null) {
            runBlocking {
                products = getFromServer()
                saveToPrefs()
            }
        } else {
            runBlocking {
                products = loadFromPrefs()
            }
        }

        addOnProductsChangedListener { saveToPrefs() }
        addOnProductChangedListener { saveToPrefs() }
    }


    private fun callOnProductChanged(product: Product) {
        onProductChangedListeners.forEach { it.onProductChanged(product) }
    }

    private fun callOnProductsChanged(products: List<Product>) {
        onProductsChangedListeners.forEach { it.onProductsChanged(products) }
    }

    private fun callOnAvailableProductsChanged(products: List<Product>) {

        onAvailableProductsChangedListeners.forEach { it.onProductsChanged(products) }
    }

    override fun getAll(): List<Product> {
        return products.toList()
    }

    override fun getAvailable(): List<Product> {
        return products.filter { it.count!! > 0 }
    }

    override fun buy(product: Product) {
        val handler = Handler()
        handler.postDelayed({
            try {
                editLock.lock()

                if (product.count!! < 1) return@postDelayed

                product.count = product.count!!.minus(1)
                callOnProductChanged(product)

                if (product.count!! == 0L) {
                    callOnAvailableProductsChanged(getAvailable())
                }
            } finally {
                editLock.unlock()
            }
        }, (3000..5000).random().toLong())
    }

    override fun add(product: Product) {
        try {
            editLock.lock()

            products.add(product)
            callOnAvailableProductsChanged(getAvailable())
            callOnProductsChanged(getAll())

        } finally {
            editLock.unlock()
        }
    }

    override fun changeProduct(product: Product, values: Product) {
        try {
            editLock.lock()

            val availableChanged =
                ((product.count!! <= 0L) && (values.count!! > 0L)) || ((product.count!! > 0L) && (values.count!! <= 0L))

            product.name = values.name
            product.price = values.price
            product.count = values.count
            callOnProductChanged(product)

            if (availableChanged) callOnAvailableProductsChanged(getAvailable())

        } finally {
            editLock.unlock()
        }
    }

    override fun addOnProductChangedListener(listener: (Product) -> Unit) {
        addOnProductChangedListener(object : OnProductChangedListener {
            override fun onProductChanged(product: Product) {
                listener(product)
            }
        })
    }

    override fun addOnProductChangedListener(listener: OnProductChangedListener) {
        onProductChangedListeners.add(listener)
    }

    override fun addOnProductsChangedListener(listener: (List<Product>) -> Unit) {
        addOnProductsChangedListener(object : OnProductsChangedListener {
            override fun onProductsChanged(products: List<Product>) {
                listener(products)
            }
        })
    }

    override fun addOnProductsChangedListener(listener: OnProductsChangedListener) {
        onProductsChangedListeners.add(listener)
    }

    override fun addOnAvailableProductsChangedListener(listener: (List<Product>) -> Unit) {
        addOnAvailableProductsChangedListener(object : OnProductsChangedListener {
            override fun onProductsChanged(products: List<Product>) {
                listener(products)
            }
        })
    }

    override fun addOnAvailableProductsChangedListener(listener: OnProductsChangedListener) {
        onAvailableProductsChangedListeners.add(listener)
    }

    suspend fun getFromServer(): MutableSet<Product> {
        val json = HttpClient().get<String>(initUrl)
        val response = gson.fromJson<ProductsInitResponse>(json, ProductsInitResponse::class.java)
        return response.products!!.toMutableSet()
    }


    private fun saveToPrefs() {
        val edit = preferences.edit()
        edit.putString(PRODUCTS_PREF, gson.toJson(ProductsInitResponse(products.toMutableList())))
        edit.apply()
    }

    fun loadFromPrefs(): MutableSet<Product> {
        val json = preferences.getString(PRODUCTS_PREF, "{}")
        val response = gson.fromJson<ProductsInitResponse>(json, ProductsInitResponse::class.java)
        return response.products!!.toMutableSet()

    }
}