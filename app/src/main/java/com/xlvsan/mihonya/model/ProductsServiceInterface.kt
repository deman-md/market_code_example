package com.xlvsan.mihonya.model

interface ProductsServiceInterface {
    fun getAll(): List<Product>
    fun getAvailable(): List<Product>
    fun buy(product: Product)
    fun add(product: Product)
    fun changeProduct(product: Product, values: Product)

    val onProductsChangedListeners: MutableList<OnProductsChangedListener>
    val onAvailableProductsChangedListeners: MutableList<OnProductsChangedListener>
    val onProductChangedListeners: MutableList<OnProductChangedListener>

    interface OnProductsChangedListener {
        fun onProductsChanged(products: List<Product>)
    }

    interface OnProductChangedListener {
        fun onProductChanged(product: Product)
    }

    fun addOnProductChangedListener(listener: OnProductChangedListener)
    fun addOnProductsChangedListener(listener: OnProductsChangedListener)
    fun addOnAvailableProductsChangedListener(listener: OnProductsChangedListener)

    fun addOnProductChangedListener(listener: (Product) -> Unit)
    fun addOnProductsChangedListener(listener: (List<Product>) -> Unit)
    fun addOnAvailableProductsChangedListener(listener: (List<Product>) -> Unit)

}
