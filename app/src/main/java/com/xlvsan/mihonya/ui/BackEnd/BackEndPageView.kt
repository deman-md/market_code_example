package com.xlvsan.mihonya.ui.BackEnd

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import com.xlvsan.mihonya.R
import com.xlvsan.mihonya.model.Product
import com.xlvsan.mihonya.model.ProductsServiceInterface

class BackEndPageView(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    var productsService: ProductsServiceInterface? = null
        set(value) {
            field = value
            productsList.products = productsService?.getAll()
            productsService?.addOnProductsChangedListener { productsList.products = it }
            productsService?.addOnProductChangedListener { productsList.update(it) }
        }

    private var addButton: Button
    private var productsList: ProductsListView

    private var productsSubpage: View
    private var productEdit: ProductEditView

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.backend_page_view, this, true)

        productsSubpage = view.findViewById(R.id.products_list_subpage)
        productEdit = view.findViewById(R.id.product_edit)

        addButton = view.findViewById(R.id.add_button)
        productsList = view.findViewById(R.id.products_list)

        addButton.setOnClickListener { addNew() }

        productEdit.setOnEditDoneListener { product: Product?, values: Product? ->
            product?.let { values?.let { productsService?.changeProduct(product, values) } }
            showList()
        }

        initList()
        showList()
    }

    private fun initList() {

        productsList.setOnItemClickListener { parent, view, position, id ->
            (view as ProductTabView).product?.let { showEdit(it) }
        }

    }


    private fun addNew() {
        productsService?.add(Product("Новый товар", "0", 0))
    }


    private fun showList() {
        productsSubpage.visibility = View.VISIBLE
        productEdit.visibility = View.INVISIBLE
    }


    private fun showEdit(product: Product) {
        productEdit.product = product

        productsSubpage.visibility = View.INVISIBLE
        productEdit.visibility = View.VISIBLE
    }
}