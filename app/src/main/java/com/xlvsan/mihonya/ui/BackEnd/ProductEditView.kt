package com.xlvsan.mihonya.ui.BackEnd

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import com.xlvsan.mihonya.R
import com.xlvsan.mihonya.model.Product

class ProductEditView(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    private var onDone: OnEditDoneListener? = null

    private var nameEdit: EditText
    private var priceEdit: EditText
    private var countEdit: EditText

    private var backButton: Button
    private var saveButton: Button


    var product: Product? = null
        set(value) {
            field = value
            if (value != null) {
                nameEdit.setText(value.name)
                priceEdit.setText(value.price)
                countEdit.setText(value.count.toString())
            }
        }

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.product_edit_view_layout, this, true)

        nameEdit = view.findViewById(R.id.name_edit)
        priceEdit = view.findViewById(R.id.price_edit)
        countEdit = view.findViewById(R.id.count_edit)

        backButton = view.findViewById(R.id.back_button)
        saveButton = view.findViewById(R.id.save_button)

        backButton.setOnClickListener { cancel() }
        saveButton.setOnClickListener {
            done(
                Product(
                    nameEdit.text.toString(),
                    priceEdit.text.toString(),
                    countEdit.text.toString().toLong()
                )
            )
        }
    }


    fun setOnEditDoneListener(listener: (product: Product?, values: Product?) -> Unit) {
        onDone = object : OnEditDoneListener {
            override fun onDone(product: Product?, values: Product?) {
                listener(product, values)
            }
        }
    }

    interface OnEditDoneListener {
        fun onDone(product: Product?, values: Product?)
    }

    private fun done(values: Product?) {
        onDone?.onDone(product, values)
    }

    private fun cancel() {
        done(null)
    }

}
