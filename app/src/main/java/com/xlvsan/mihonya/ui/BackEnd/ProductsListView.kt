package com.xlvsan.mihonya.ui.BackEnd

import android.content.Context
import android.util.AttributeSet
import android.widget.ListView
import com.xlvsan.mihonya.model.Product

class ProductsListView(context: Context, attrs: AttributeSet?) : ListView(context, attrs) {

    var products: List<Product>? = null
        set(value) {
            field = value
            adapter = value?.toMutableList()?.let { ProductTabAdapter(context, it) }
        }

    fun update(product: Product) {
        val idx = products?.indexOf(product)
        idx?.let {
            val view = getChildAt(idx - firstVisiblePosition) as ProductTabView
            view.product = product
        }
    }
}