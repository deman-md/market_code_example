package com.xlvsan.mihonya.ui.StoreFront

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.Button
import android.widget.FrameLayout
import com.xlvsan.mihonya.R
import com.xlvsan.mihonya.model.Product

class ProductView(context: Context, attrs: AttributeSet? = null) : FrameLayout(context, attrs) {
    private var nameInfoLine: InfoLineView
    private var priceInfoLine: InfoLineView
    private var countInfoLine: InfoLineView
    private var buyButton: Button

    private var onBuyListener: OnProductBuyListener? = null

//    var productsService: ProductsServiceInterface? = null
//        set(value) {
//            field = value
//            productsService?.addOnProductChangedListener { if (product == it) updateView(product) }
//        }

    var product: Product? = null
        set(value) {
            field = value
            updateView(value)
        }

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.front_product_view_layout, this, true)
        nameInfoLine = view.findViewById(R.id.product_name_info_line)
        priceInfoLine = view.findViewById(R.id.price_info_line)
        countInfoLine = view.findViewById(R.id.count_info_line)
        buyButton = view.findViewById(R.id.buy_button)

        initUi()
        initBehaviour()
    }

    private fun initUi() {
        priceInfoLine.name = "Цена"
        countInfoLine.name = "Колличество"
    }

    private fun initBehaviour() {
        buyButton.setOnClickListener {
            product?.let { onBuyListener?.onProductBuy(it) }
        }
    }

    private fun updateView(value: Product?) {
        nameInfoLine.name = value?.name
        priceInfoLine.value = "${value?.price}руб."
        countInfoLine.value = "${value?.count}шт."
    }

    fun setOnProductBuyListener(listener: OnProductBuyListener) {
        onBuyListener = listener
    }

    fun setOnProductBuyListener(listener: (Product) -> Unit) {
        setOnProductBuyListener(object : OnProductBuyListener {
            override fun onProductBuy(product: Product) {
                listener(product)
            }
        })
    }


    interface OnProductBuyListener {
        fun onProductBuy(product: Product)
    }

}