package com.xlvsan.mihonya.ui.StoreFront

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.xlvsan.mihonya.model.Product

class ProductPageAdapter(private val context: Context, var products: List<Product>) : PagerAdapter() {

    private var onBuyListener: ProductView.OnProductBuyListener? = null

    override fun isViewFromObject(view: View, item: Any): Boolean {
        if (view is ProductView) {
            view.product = view.product
        }
        return view === item
    }

    override fun getCount(): Int {
        return products.count()
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = ProductView(context)
        view.setOnProductBuyListener {
            onBuyListener?.onProductBuy(it)
        }
        view.product = products[position]
        container.addView(view)
        return view
    }

    override fun destroyItem(collection: ViewGroup, position: Int, item: Any) {
        collection.removeView(item as View)
    }

    fun setOnProductBuyListener(listener: ProductView.OnProductBuyListener) {
        onBuyListener = listener
    }

    fun setOnProductBuyListener(listener: (Product) -> Unit) {
        setOnProductBuyListener(object : ProductView.OnProductBuyListener {
            override fun onProductBuy(product: Product) {
                listener(product)
            }
        })
    }
}