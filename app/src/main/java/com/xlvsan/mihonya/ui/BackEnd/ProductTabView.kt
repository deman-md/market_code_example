package com.xlvsan.mihonya.ui.BackEnd

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.TextView
import com.xlvsan.mihonya.R
import com.xlvsan.mihonya.model.Product
import com.xlvsan.mihonya.model.ProductsServiceInterface


class ProductTabView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    private var nameTextView: TextView
    private var countTextView: TextView

    var productsService: ProductsServiceInterface? = null
        set(value) {
            field = value
            productsService?.addOnProductChangedListener { if (product == it) updateView(product) }
        }

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.product_tab_view_layout, this, true)
        nameTextView = view.findViewById(R.id.name_text_view)
        countTextView = view.findViewById(R.id.count_text_view)
    }


    private var name: String? = null
        set(value) {
            field = value
            nameTextView.text = value
        }

    private var count: String? = null
        set(value) {
            field = value
            countTextView.text = value
        }

    var product: Product? = null
        set(value) {
            field = value
            updateView(value)
        }

    private fun updateView(value: Product?) {

        if (value != null) {
            name = value.name
            count = "${value.count}шт."
        }
    }
}
