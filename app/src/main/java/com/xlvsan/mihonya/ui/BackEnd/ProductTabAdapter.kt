package com.xlvsan.mihonya.ui.BackEnd

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.xlvsan.mihonya.model.Product

class ProductTabAdapter(context: Context, objects: MutableList<Product>) :
    ArrayAdapter<Product>(context, 0, objects) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = ProductTabView(context, null)
        view.product = getItem(position)
        return view
    }

}