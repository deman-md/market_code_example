package com.xlvsan.mihonya.ui.StoreFront

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.TextView
import com.xlvsan.mihonya.R


class InfoLineView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    private var nameTextView: TextView
    private var valueTextView: TextView

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.info_line_view_layout, this, true)
        nameTextView = view.findViewById(R.id.name_text_view)
        valueTextView = view.findViewById(R.id.value_text_view)
    }

    var name: String? = null
        set(value) {
            field = value
            nameTextView.text = value
        }

    var value: String? = null
        set(value) {
            field = value
            valueTextView.text = value
        }

}
