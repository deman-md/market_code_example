package com.xlvsan.mihonya.ui.StoreFront

import android.content.Context
import android.util.AttributeSet
import androidx.viewpager.widget.ViewPager
import com.xlvsan.mihonya.model.Product
import com.xlvsan.mihonya.model.ProductsServiceInterface

class ProductsPagesView(context: Context, attrs: AttributeSet?) : ViewPager(context, attrs) {


    var productsService: ProductsServiceInterface? = null
        set(value) {
            field = value
            products = productsService?.getAvailable()
            productsService?.addOnAvailableProductsChangedListener { products = it }
//            productsAdapter?.productsService = value
        }

    var products: List<Product>? = null
        set(value) {
            field = value
            val productsAdapter = field?.let { ProductPageAdapter(context, it) }
            adapter = productsAdapter



            productsAdapter?.setOnProductBuyListener {
                productsService?.buy(it)
            }

            productsService?.addOnProductChangedListener { adapter!!.notifyDataSetChanged() }
        }

    init {


    }
//
//    class asyncBuyer(productsService: ProductsServiceInterface) : AsyncTask<Product, Void, Void?>() {
//        var productsService: ProductsServiceInterface? = null
//
//        init {
//            this.productsService = productsService
//        }
//
//        override fun doInBackground(vararg product: Product): Void? {
//            productsService?.buy(product[0])
//            return null
//        }
//
//    }
}