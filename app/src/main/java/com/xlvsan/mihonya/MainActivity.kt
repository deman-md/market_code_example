package com.xlvsan.mihonya

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import com.xlvsan.mihonya.model.ProductService
import com.xlvsan.mihonya.model.ProductsServiceInterface
import com.xlvsan.mihonya.ui.BackEnd.BackEndPageView
import com.xlvsan.mihonya.ui.StoreFront.ProductsPagesView

class MainActivity : AppCompatActivity() {


    var productsService: ProductsServiceInterface?=null

    var storeFront: ProductsPagesView? = null
    var backEnd: BackEndPageView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        productsService = ProductService(applicationContext)

        storeFront = findViewById(R.id.store_front)
        storeFront?.productsService = productsService


        backEnd = findViewById(R.id.back_end)
        backEnd?.productsService = productsService


        val storeFrontFrame = findViewById<FrameLayout>(R.id.store_front_frame)
        val backEndFrame = findViewById<FrameLayout>(R.id.back_end_frame)

        val storeFrontButton = findViewById<Button>(R.id.store_front_button)
        val backEndButton = findViewById<Button>(R.id.back_end_button)

        storeFrontButton.setOnClickListener {
            storeFrontFrame.visibility = View.VISIBLE
            backEndFrame.visibility = View.INVISIBLE
        }

        backEndButton.setOnClickListener {
            storeFrontFrame.visibility = View.INVISIBLE
            backEndFrame.visibility = View.VISIBLE
        }


    }


}
